<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;


class Mobile
{

	protected $provider;
	
	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}


	public function makeCallByName($name = '')
	{
		if( empty($name) ) return;

		$contact 	= ContactService::findByName($name);
		$data 		= [];
		$data['status'] 		= false;
		$data['message'] 		= 'The Name not exists in contacts';


		

		if (empty($contact)) return $data;

		if (empty(trim($contact['number_phone'])) || trim($contact['number_phone']) == null)
		{
			$data['message'] 		= 'The contact phone is empty';
			return $data;
		}

		$validate = ContactService::validateNumber($contact['number_phone']);
		if (empty($validate))
		{
			$data['message'] 		= 'The phone number is an invalid value';
			return $data;
		}


		$this->sendMessageTwilio($contact['number_phone']);

		$this->provider->dialContact($contact);

		return $this->provider->makeCall();
	}


	public function sendMessageTwilio($number)
	{
			$sid = "ACXXXXXX";  
			$token = "YYYYYY";
			$client = new \Twilio\Rest\Client($sid, $token);

			// Use the Client to make requests to the Twilio REST API
			$client->messages->create(
			// The number you'd like to send the message to
			'+51'.$number,
			[
			'from' => '+15017250604', // the number checkout
			'body' => "Hey, this message example with Clarence"
			]
			);
	}


}
